class Stack {
    top;
    arr;
    constructor() {
        this.arr = [];
        this.top = -1;
    }
    push(val) {
        this.top++;
        this.arr[this.top] = val;
    }

    pop() {
        if (this.top == -1) {
            console.log('Stack underflow');
            return -1;
        } else {
            let poppedEl = this.arr[this.top];
            this.top--;
            return poppedEl;
        }
    }

    topOfStack() {
        return this.arr[this.top];
    }

    sizeOfStack() {
        return this.top + 1;
    }

    display() {
    
        for (let i = this.top; i >= 0; i--) {
            console.log(this.arr[i]);
        }
      
    }
}

const stack = new Stack();
stack.push(1);
stack.push(2);
stack.push(3);
stack.push(4);
stack.display();
stack.pop()
stack.pop()
stack.pop()
stack.pop()
stack.pop()
// console.log('popped element is ', stack.pop())
console.log('top of the stack is ', stack.topOfStack());
console.log('size of the stack is ', stack.sizeOfStack());
stack.display();