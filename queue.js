class Queue {
    size;
    arr;
    front ;
    rear;
    constructor(){
        this.front = 0;
        this.rear = -1;
       this.size = 0;
        this.arr = [];
    }
    
    enqueue(val){
        this.rear = this.front + this.size;
        this.arr[this.rear] = val;
        this.size++;
    }
    dequeue(){
        let val = this.arr[this.front];
        this.front++;
        this.size--;
        return val;
    }
    sizeOfQueue(){
      return this.size;
    }
    display(){
        for(let i = 0; i < this.size; i++){
           console.log(this.arr[i]);
        }
    }
}

const queue = new Queue();
queue.enqueue(1);
queue.enqueue(2);
queue.enqueue(3);
queue.dequeue();
queue.display();
 console.log('size of the queue is ',queue.sizeOfQueue())