class Stack{
    top;
    arr;
    constructor(size){
        this.arr = new Array(size);
        this.top = -1;
    }
    push(val){
        if(this.top == this.arr.length - 1){
            console.log('Stack overflow');
        }else{
            this.top++;
            this.arr[this.top] = val;
        }
    }

    pop(){
        if(this.top == -1){
            console.log('Stack underflow');
            return -1;
        }else{
           let poppedEl =  this.arr[this.top];
           this.top--;
            return poppedEl;
        }
    }

    topOfStack(){
      return this.arr[this.top];
    }

    sizeOfStack(){
        return this.top + 1;
    }

    display(){
        for(let i = this.top; i >= 0; i--){
            console.log(this.arr[i]);
        }
    }
}

const stack1 = new Stack(4);
stack1.push(1);
stack1.push(2);
stack1.push(3);
stack1.push(4);
stack1.push(5);
stack1.display();
// stack1.pop()
// console.log('popped element is ', stack1.pop())
console.log('top of the stack is ',stack1.topOfStack());
console.log('size of the stack is ',stack1.sizeOfStack());